const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

const category_schema = Schema({
    name: {
        type: String,
        required: true
    },
    id: {
        type: Number,
        required: true
    },
    parent: {
        type: Number,
        default: null
    },
    active: {
        type: Boolean,
        default: 1
    },
    created_at: {
        type: Date,
        default: new Date()
    },
    updated_at: {
        type: Date,
        default: new Date()
    }
}, {
    versionKey: false
});

const product_schema = Schema({
    id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true
    },
    category: {
        type: Array,
        default: []
    },
    active: {
        type: Boolean,
        default: 1
    },
    created_at: {
        type: Date,
        default: new Date()
    },
    updated_at: {
        type: Date,
        default: new Date()
    }
}, {
    versionKey: false
});

const counter_schema = Schema({
    _id: {
        type: String,
        required: true
    },
    seq: {
        type: Number,
        default: 0
    },
    updated_at: {
        type: Date,
        default: new Date()
    }
}, {
    versionKey: false
});

const category = mongoose.model('categories', category_schema),
    product = mongoose.model('products', product_schema),
    counter = mongoose.model('counters', counter_schema);

// Creating counters for CategoryID if not present
const getCategoryId = () => {
    return counter.findByIdAndUpdate({
        _id: 'category'
    }, {
        $inc: {
            seq: 1
        }
    }, {
        upsert: true,
        new: true,
        safe: true
    });
}

const getProductId = () => {
    return counter.findByIdAndUpdate({
        _id: 'product'
    }, {
        $inc: {
            seq: 1
        }
    }, {
        upsert: true,
        new: true,
        safe: true
    });
}

module.exports = {
    category,
    product,
    getCategoryId,
    getProductId
}