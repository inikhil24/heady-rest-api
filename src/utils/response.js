exports.ERROR = function(msg) {
    this.jsonresponse = {
        code: 500,
        response: (msg) ? msg : 'some error occurred'
    };
    this.send = send;
};
exports.SUCCESS = function(msg, error = false) {
    this.jsonresponse = {
        code: 200,
        error: error ? true : false,
        response: (msg) ? msg : 'success'
    };
    this.send = send;
};
exports.CREATED = function(msg) {
    this.jsonresponse = {
        code: 201,
        response: (msg) ? msg : 'success'
    };
    this.send = send;
};
exports.ACCEPTED = function(msg) {
    this.jsonresponse = {
        code: 202,
        response: (msg) ? msg : 'success'
    };
    this.send = send;
};
exports.BAD_REQUEST = function(msg) {
    this.jsonresponse = {
        code: 400,
        response: (msg) ? msg : 'bad request'
    };
    this.send = send;
};
exports.AUTH_FAILED = function(msg) {
    this.jsonresponse = {
        code: 401,
        response: (msg) ? msg : 'auth failure'
    };
    this.send = send;
};
exports.NO_RECORD_FOUND = function(msg) {
    this.jsonresponse = {
        code: 404,
        response: (msg) ? msg : 'no record found'
    };
    this.send = send;
};
exports.NOT_FOUND = function(msg) {
    this.jsonresponse = {
        code: 404,
        response: (msg) ? msg : 'not found'
    };
    this.send = send;
};
exports.SERVER_TIMEDOUT = function(msg) {
    this.jsonresponse = {
        code: 408,
        response: (msg) ? msg : 'server timed out'
    };
    this.send = send;
};
exports.CONFLICT = function(msg) {
    this.jsonresponse = {
        code: 409,
        response: (msg) ? msg : 'conflict'
    };
    this.send = send;
};

let send = function(res, optional_json) {
    if (optional_json)
        this.jsonresponse[optional_json.name] = optional_json.data;
    res.status(this.jsonresponse.code).json(this.jsonresponse);
};