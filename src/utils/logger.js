const bunyan = require("bunyan"),
    BunyanFileRotation = require("bunyan-rotating-file-stream"),
    fs = require("fs");

module.exports = (() => {
    if (!fs.existsSync(global.config.logger.path)) fs.mkdirSync(global.config.logger.path);

    return bunyan.createLogger({
        name: "heady_rest",
        serializers: bunyan.stdSerializers,
        streams: [{
            stream: new BunyanFileRotation({
                path: `${global.config.logger.path}/%Y%m%d.log`,
                period: "1d",
                level: 'warn',
                rotateExisting: true
            })
        }, {
            level: "error",
            stream: process.stdout
        }]
    });
})();