const Joi = require('joi');

const categorySchema = {
    "get": Joi.object().keys({
        id: Joi.number().integer().positive().greater(0).label('Category Id')
    }),
    "post": Joi.object().keys({
        name: Joi.string().regex(/^[a-zA-Z]*$/).lowercase().label('Category name').required().error(errors => {
            return {
                template: "is required and must be in string"
            };
        }),
        parent: Joi.number().integer().positive().greater(0).label('Parent Id').error(errors => {
            return {
                template: "must be an integer"
            };
        })
    }),
    "put": Joi.object().keys({
        name: Joi.string().regex(/^[a-zA-Z]*$/).lowercase().label('Category name').required().error(errors => {
            return {
                template: "is required and must be in string"
            };
        }),
        id: Joi.number().integer().positive().greater(0).label('Category Id').required().error(errors => {
            return {
                template: "is required and must be a number"
            };
        })
    })
}

const productSchema = {
    "post": Joi.object().keys({
        name: Joi.string().lowercase().label('Product name').required().error(errors => {
            return {
                template: "is required and must be in string"
            };
        }),
        price: Joi.number().integer().positive().greater(0).label('Product price').required().error(errors => {
            return {
                template: "is required and must be a number"
            };
        }),
        category: Joi.array().items(Joi.number().integer().positive().greater(0).required()).unique().min(1).required().label("Category").error(errors => {
            return {
                template: "is required and should a number in array"
            }
        })
    }),
    "get": Joi.object().keys({
        category_id: Joi.number().integer().positive().greater(0).label('Category Id').required()
    }),
    "put": Joi.object().keys({
        id: Joi.number().integer().positive().greater(0).label('Product Id').required(),
        name: Joi.string().lowercase().label('Product name').error(errors => {
            return {
                template: "is must be a string"
            };
        }),
        price: Joi.number().integer().positive().greater(0).label('Product price').error(errors => {
            return {
                template: "must be a number"
            };
        }),
        category: Joi.array().items(Joi.number().integer().positive().greater(0).required()).unique().min(1).label("Category").error(errors => {
            return {
                template: "should be a number in array"
            }
        })
    }),
}


module.exports = {
    validateInput: (input, state, type = "category") => {
        return new Promise((resolve, reject) => {
            return Joi.validate(
                input,
                (type == "product" ? productSchema[state] : categorySchema[state]), {
                    abortEarly: true,
                    allowUnknown: false,
                    escapeHtml: false
                },
                (error, data) => {
                    if (error)
                        return reject(new Error(error.details[0].message))

                    return resolve(data);
                }
            )
        })
    }
}