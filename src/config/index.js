const path = require("path");

global.config = {
    "APP_PORT": 3315,
    "db": {
        "uri": "mongodb://127.0.0.1:27017/heady"
    },
    "logger": {
        "path": path.join(__dirname, "../logs") 
    }
}