const bunyan = require("bunyan"),
    BunyanFileRotation = require("bunyan-rotating-file-stream"),
    config = require("./index"),
    fs = require("fs");

module.exports = (() => {
    if (!fs.existsSync(config.logger.path)) fs.mkdirSync(config.logger.path);

    return bunyan.createLogger({
        name: "heady_rest",
        serializers: bunyan.stdSerializers,
        streams: [{
            stream: new BunyanFileRotation({
                path: `${config.logger.path}/%Y%m%d.log`,
                period: "1d",
                level: 'warn',
                rotateExisting: true
            })
        }, {
            level: "error",
            stream: process.stdout
        }]
    });
})();