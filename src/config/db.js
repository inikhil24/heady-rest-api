'use strict';

const mongoose = require("mongoose");
mongoose.Promise = require("bluebird");

module.exports = (() => {
    mongoose.connect(global.config.db.uri, {
            useNewUrlParser: true
        })
        .then(con => {
            console.log(`Successfully connected to MongoDB`);
        })
        .catch(error => {
            console.error(`Error connecting to MongoDB ${error}`);
            process.exit(0);
        });
})();