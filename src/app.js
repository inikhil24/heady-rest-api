const app = require("express")(),
    express = require("express"),
    bodyparser = require("body-parser"),
    config = require("./config"),
    __res = require("./utils/response"),
    logger = require("./utils/logger"),
    swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./docs/swagger.json');
require("./config/db");

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    const allowOrigin = req.headers.origin || "http://localhost:" + global.config.APP_PORT;

    res.setHeader("Access-Control-Allow-Origin", allowOrigin);
    res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');

    next();
});

// ========= LOG EVERY REQUEST ==========
app.use((req, res, next) => {
    logger.info({
        request: req
    })
    next();
})

// ============= ROUTES =============
const route = require("./routes");
app.use("/api/v1", route.category);
app.use("/api/v1", route.product);

//=========== API DOCS ===============
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


// ================ INVALID ROUTES =====================
app.all('*', (req, res) => {
    return new __res.NOT_FOUND("not found").send(res);
});

// ============= FALLBACK ERROR HANDLER ==============================
app.use((err, req, res, next) => {
    if (err instanceof SyntaxError) {
        return new __res.ERROR("invalid json").send(res);
    } else {
        logger.error({
            error: err
        });
        return new __res.ERROR(err.message).send(res);
    }
});

app.listen(global.config.APP_PORT, err => {
    if (!err)
        console.log(`REST Server Started at port ${global.config.APP_PORT} \nSwagger docs http://127.0.0.1:${global.config.APP_PORT}/docs`);
})