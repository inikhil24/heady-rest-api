const category = require("./category"),
    product = require("./product");


module.exports = {
    category,
    product
}