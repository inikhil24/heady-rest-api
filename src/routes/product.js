const express = require("express"),
    router = express.Router(),
    db = require("../utils/db_schema"),
    validation = require("../utils/validation"),
    __res = require("../utils/response");

router.post("/product", (req, res, next) => {
    let input = null;
    validation.validateInput(req.body, "post", "product")
        .then(validated => {
            input = validated;
            return db.category.find({
                id: {
                    $in: input.category
                }
            }, {
                _id: 0,
                id: 1
            })
        })
        .then(categoryList => {
            if (input.category.length != categoryList.length) {
                let categoryIds = categoryList.reduce((final, item) => {
                    return final.concat(item["id"]);
                }, []);
                let invalidIds = input.category.filter(category => {
                    return categoryIds.indexOf(category) === -1
                });

                throw new Error(`invalid category ${invalidIds.join(', ')}`);
            }
            return db.getProductId()
                .then(product => {
                    input["id"] = product.seq;
                    return input;
                })
        })
        .then(input => db.product.create(input))
        .then(product => {
            return new __res.SUCCESS({
                "msg": "product added",
                id: product.id
            }).send(res);
        })
        .catch(error => {
            return new __res.BAD_REQUEST(error.message).send(res);
        });
});

router.get("/product/:category_id?", (req, res, next) => {
    validation.validateInput({
            category_id: req.params.category_id
        }, "get", "product")
        .then(validated => {
            return db.product.find({
                category: {
                    $elemMatch: {
                        $eq: validated.category_id
                    }
                }
            }, {
                _id: 0,
                name: 1,
                price: 1,
                category: 1
            });
        })
        .then(product => {
            return new __res.SUCCESS(product).send(res);
        })
        .catch(error => {
            return new __res.BAD_REQUEST(error.message).send(res);
        });
});

router.put("/product", (req, res, next) => {
    let input = null;
    let updateQuery = {};
    validation.validateInput(req.body, "put", "product")
        .then(validated => {
            input = validated;
            if (input.name)
                updateQuery["name"] = input.name
            if (input.price)
                updateQuery["price"] = input.price

            if (input.category)
                return db.category.find({
                        id: {
                            $in: input.category
                        }
                    }, {
                        _id: 0,
                        id: 1
                    })
                    .then(categoryList => {
                        if (input.category.length != categoryList.length) {
                            let categoryIds = categoryList.reduce((final, item) => {
                                return final.concat(item["id"]);
                            }, []);
                            let invalidIds = input.category.filter(category => {
                                return categoryIds.indexOf(category) === -1
                            });

                            throw new Error(`invalid category ${invalidIds.join(', ')}`);
                        }
                        if (input.category)
                            updateQuery["category"] = input.category
                    })
        })
        .then(() => {
            return db.product.update({
                id: input.id
            }, {
                $set: updateQuery
            }, {
                upsert: false,
                multi: true
            });
        })
        .then(update => {
            return new __res.SUCCESS(update.nModified > 0 ? 'product updated' : 'product not found').send(res);
        })
        .catch(error => {
            return new __res.BAD_REQUEST(error.message).send(res);
        });
});


module.exports = router;