const express = require("express"),
    router = express.Router(),
    db = require("../utils/db_schema"),
    validation = require("../utils/validation"),
    __res = require("../utils/response");

router.get("/category", (req, res, next) => {
    db.category.aggregate([{
            $graphLookup: {
                from: "categories",
                startWith: "$id",
                connectFromField: "id",
                connectToField: "parent",
                as: "childs",
                depthField: "sub_childs"
            }
        }, {
            $project: {
                "name": 1,
                "_id": 0,
                "parent": 1,
                "child_categories": "$childs.name",
            }
        }])
        .then(list => {
            return new __res.SUCCESS(list).send(res);
        })
        .catch(error => {
            return new __res.BAD_REQUEST(error.message).send(res);
        });
})

router.post("/category", (req, res, next) => {
    let input = null;
    validation.validateInput(req.body, "post")
        .then(() => {
            input = req.body;
            return db.category.findOne({
                name: input.name,
                active: 1
            })
        })
        .then(category => {
            if (category !== null)
                throw new Error(`category already exists`);

            if (input.parent)
                return db.category.findOne({
                        id: input.parent,
                        active: 1
                    })
                    .then(parent => {
                        if (parent === null)
                            throw new Error(`no such parent exists`);
                    })
        })
        .then(() => db.getCategoryId())
        .then(CategoryId => {
            let doc = {
                name: input.name,
                id: CategoryId.seq,
                parent: input.parent ? parseInt(input.parent) : null
            };
            return db.category.create(doc);
        })
        .then(category => {
            return new __res.SUCCESS({
                msg: "category added",
                id: category.id
            }).send(res);
        })
        .catch(error => {
            return new __res.BAD_REQUEST(error.message).send(res);
        })
});

router.put("/category", (req, res, next) => {
    let input = null;
    let updateQuery = {};
    validation.validateInput(req.body, "put")
        .then(() => {
            input = req.body;
            return db.category.findOne({
                    id: input.id,
                    active: 1
                })
                .then(category => {
                    if (category === null)
                        throw new Error("category not found");
                })
        })
        .then(() => {
            return db.category.findOne({
                    name: input.name,
                    active: 1,
                    id: {
                        $ne: input.id
                    }
                })
                .then(category => {
                    if (category !== null)
                        throw new Error("category name already exists");
                    updateQuery["name"] = input.name;
                })
        })
        .then(() => {
            if (Object.keys(updateQuery).length > 0) {
                updateQuery["updated_at"] = new Date();
                return db.category.update({
                    id: input.id,
                    active: 1
                }, {
                    $set: updateQuery
                }, {
                    upsert: false,
                    multi: false
                })
            }
        })
        .then(updtedCategory => {
            return new __res.SUCCESS(updtedCategory ? 'category updated' : 'category not updated').send(res);
        })
        .catch(error => {
            return new __res.BAD_REQUEST(error.message).send(res);
        })
});

router.delete("/category/:id?", (req, res, next) => {
    let categoryId = null;
    console.log('-->',req.params.id);
    validation.validateInput({
            id: parseInt(req.params.id)
        }, "get")
        .then(() => {
            categoryId = req.params.id
            return db.category.findOne({
                id: parseInt(categoryId),
                active: 1
            })
        })
        .then(category => {
            if (category === null)
                throw new Error(`category doesn't exists`);

            return db.category.update({
                id: parseInt(categoryId),
                active: 1
            }, {
                $set: {
                    active: 0
                }
            }, {
                multi: false,
                upsert: false
            })
        })
        .then(category => {
            return new __res.SUCCESS("category deleted").send(res);
        })
        .catch(error => {
            return new __res.BAD_REQUEST(error.message).send(res);
        })
});


module.exports = router;